
# IEX Scrapper app

## Run app
```bash
make run
```
App will build all relevant configuration and will expose http service to port `8010` 

## HTTP Interface

| Methods 	| Path    	| Required params 	| Example                         	|
|---------	|---------	|-----------------	|---------------------------------	|
| GET     	| getData 	| symbol          	| /getData?symbol=appl&symbol=fbs 	|


## Building

Requirements:
 - `docker`

Build app for linux
````bash
make build
````
Run Unit tests  
````bash
make test
````

 If you want your database to be persistent please add
`/var/lib/mysql` to your `docker-compose.yml` file in `db` section otherwise on every run data will be default o

In this codebase I'm using `IEX_SECRET_TOKEN` as `ENV` param and it can be controlled also from `docker-compose` file.


> Not done:
> > Unit tests
> > Other tests
