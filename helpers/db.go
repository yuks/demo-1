package helpers

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/jmoiron/sqlx"
	"log"
)

var DB *sqlx.DB
var GormDB *gorm.DB

func ConnectDB() {
	var err error

	DB, err = sqlx.Connect("mysql", Config.Database.Dsn)
	if err != nil {
		log.Println("UPS")
		log.Fatalf(err.Error())
	}
	DB.SetMaxOpenConns(Config.Database.MaxOpenConnections)
	DB.SetMaxIdleConns(Config.Database.MaxIddleConnections)
	// DB.SetConnMaxLifetime(1 * time.Minute)
}

func ConnectGorm() {
	var err error

	dsn := fmt.Sprintf("%s?charset=utf8&parseTime=True&loc=Local", Config.Database.Dsn)
	GormDB, err = gorm.Open("mysql", dsn)

	if Config.App.Debug {
		GormDB.LogMode(true)
	}

	if err != nil {
		log.Println("UPS")
		log.Fatalf(err.Error())
	}
}
