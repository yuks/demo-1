package helpers

import (
	"github.com/BurntSushi/toml"
)

type configStruct struct {
	App struct {
		Port            int    `toml:"port"`
		Debug           bool   `toml:"debug"`
		BaseRedirectUrl string `toml:"base_redirect_urls"`
		MinNumber       int    `toml:"min_number"`
	} `toml:"app"`
	Database struct {
		Dsn                 string `toml:"dsn"`
		MaxOpenConnections  int    `toml:"max_open_connections"`
		MaxIddleConnections int    `toml:"max_idle_connections"`
	} `toml:"database"`
}

var Config configStruct

func LoadConfig() {
	toml.DecodeFile("config.toml", &Config)
}
