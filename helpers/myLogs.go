package helpers

import (
	"github.com/sirupsen/logrus"
)

var MyLog = logrus.New()

func init() {

	MyLog.SetLevel(logrus.InfoLevel)
	MyLog.SetFormatter(&logrus.TextFormatter{ForceColors: true})
}
