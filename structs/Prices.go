package structs

type Prices struct {
	ID          uint64  `db:"id" json:"-" gorm:"column:id"`
	CompanyID   int64   `db:"company_id" json:"-" gorm:"column:company_id"`
	ScrapedTime string  `db:"scrape_time" json:"scraped_time" gorm:"column:scrape_time"`
	Price       float64 `db:"current_price" json:"price" gorm:"column:current_price"`
}
