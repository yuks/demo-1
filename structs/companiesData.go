package structs

type CompaniesDataResult struct {
	CompanyName string   `json:"company_name"`
	CompanyLogo string   `json:"company_logo"`
	Prices      []Prices `json:"prices"`
}
