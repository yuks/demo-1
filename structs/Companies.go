package structs

import "iex-scrapper/helpers"

type Companies struct {
	Id              uint64 `db:"id" json:"-"`
	Symbol          string `db:"symbol" json:"symbol"`
	CompanyName     string `db:"company_name" json:"company_name"`
	LogoURL         string `db:"logo_url" json:"logo_url"`
	LastScrapedTime string `db:"last_scraped" json:"last_scraped_time"`
}

// Update logo of company
func (el *Companies) UpdateLogo(LogoUrl string) error {
	sql := `update scraped_companies set logo_url = ? where id = ? limit 1`
	_, er := helpers.DB.Exec(sql, LogoUrl, el.Id)
	return er
}
