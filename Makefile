# Application name
BINARY_NAME=mybinary

BUILD_IMAGE ?= golang:1.12.7

CUR_DIR = $(CURDIR)

# Check for executables
EXECUTABLES = docker
K := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

# HELP
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

# Build task
.PHONY: build
build: ## Build application for linux in docker container
	@docker run -v $(CUR_DIR):/go/src/iex-scrapper -w /go/src/iex-scrapper --rm $(BUILD_IMAGE) go build -v -o $(BINARY_NAME) .	
	@echo ""
	@echo "Binary name: $(BINARY_NAME) "
	@echo ""

# Run task
.PHONY: run
run: ## Run current app inside of docker
	@docker-compose up --build -d
	@echo "App will be available in few seconds"

clean: ## Clean the generated/compiles files
	rm -f $(BINARY_NAME)

