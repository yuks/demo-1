package cron

import (
	cr "gopkg.in/robfig/cron.v3"
	"iex-scrapper/modules"
	"log"
)

func Start() {
	log.Println("____")
	c := cr.New()
	c.AddFunc("@every 30s", func() {
		log.Println("Every 30s check")
		modules.ScrapAndParse()
	})
	c.Run()
}
