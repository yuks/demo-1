-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.27 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5646
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table iex-scrapper.scraped_companies
CREATE TABLE IF NOT EXISTS `scraped_companies` (
                                                   `id` bigint(11) NOT NULL AUTO_INCREMENT,
                                                   `symbol` varchar(50) NOT NULL,
                                                   `company_name` varchar(250) NOT NULL,
                                                   `logo_url` varchar(500) NOT NULL,
                                                   `scrape_period` int(11) NOT NULL DEFAULT '60' COMMENT ' in seconds ',
                                                   `last_scraped` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                                   PRIMARY KEY (`id`),
                                                   UNIQUE KEY `symbol` (`symbol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table iex-scrapper.scraped_companies: ~4 rows (approximately)
/*!40000 ALTER TABLE `scraped_companies` DISABLE KEYS */;
INSERT INTO `scraped_companies` (`id`, `symbol`, `company_name`, `logo_url`, `scrape_period`, `last_scraped`) VALUES
(1, 'aapl', 'Apple', 'https://storage.googleapis.com/iex/api/logos/AAPL.png', 60, '2019-08-08 14:26:49'),
(2, 'fb', 'Facebook', 'https://storage.googleapis.com/iex/api/logos/FB.png', 60, '2019-08-08 14:26:49'),
(3, 'GOOGL', 'Google', 'https://storage.googleapis.com/iex/api/logos/GOOGL.png', 60, '2019-08-08 14:26:49'),
(4, 'AABA', 'Altababa', 'https://storage.googleapis.com/iex/api/logos/AABA.png', 60, '2019-08-08 14:26:49');
/*!40000 ALTER TABLE `scraped_companies` ENABLE KEYS */;

-- Dumping structure for table iex-scrapper.scraped_prices
CREATE TABLE IF NOT EXISTS `scraped_prices` (
                                                `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
                                                `company_id` bigint(20) NOT NULL,
                                                `scrape_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                                `current_price` float NOT NULL DEFAULT '0',
                                                PRIMARY KEY (`id`),
                                                KEY `FK__scraped_companies` (`company_id`),
                                                KEY `scrape_time` (`scrape_time`),
                                                CONSTRAINT `FK__scraped_companies` FOREIGN KEY (`company_id`) REFERENCES `scraped_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table iex-scrapper.scraped_prices: ~0 rows (approximately)
/*!40000 ALTER TABLE `scraped_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `scraped_prices` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
