package main

import (
	"iex-scrapper/cron"
	"iex-scrapper/helpers"
	"iex-scrapper/modules/http_service"
	//"iex-scrapper/modules"
)

func init() {
	helpers.LoadConfig()
	helpers.ConnectDB()
	helpers.ConnectGorm()
}

func main() {
	go cron.Start()
	http_service.Start()
}
