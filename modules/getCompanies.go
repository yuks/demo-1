package modules

import (
	"iex-scrapper/helpers"
	"iex-scrapper/structs"
	"log"
	"sync"
)

type CompaniesFilter struct {
	Symbols []string `db:"symbol"`
}

func GetCompanies(filter CompaniesFilter) (companies []structs.Companies) {
	// get all filtered companies
	db := helpers.GormDB.
		Table("scraped_companies")
	if len(filter.Symbols) != 0 {
		db = db.Where("symbol IN (?)", filter.Symbols)
	}

	db.Find(&companies)

	var result []structs.CompaniesDataResult
	var wg sync.WaitGroup
	wg.Add(len(companies))
	for _, el := range companies {
		go func(result *[]structs.CompaniesDataResult, el structs.Companies) {
			defer wg.Done()
			db2 := helpers.GormDB.Debug().Table("scraped_prices").
				Select("company_id,scrape_time,current_price").
				Where("company_id = ?", el.Id).
				Order("scrape_time DESC")
			var prices []structs.Prices
			db2.Find(&prices)
			*result = append(*result, structs.CompaniesDataResult{
				CompanyName: el.CompanyName,
				CompanyLogo: el.LogoURL,
				Prices:      prices,
			})
		}(&result, el)
	}
	wg.Wait()


	log.Println(result)

	return
}

// get list of companies which will be scrapped
func GetCompaniesForScrape(filter CompaniesFilter) []structs.Companies {
	sql := `select * from scraped_companies`
	var companies []structs.Companies
	helpers.DB.Unsafe().Select(&companies, sql)
	return companies
}
