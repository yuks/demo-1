package modules

import (
	iex "github.com/jonwho/go-iex"
	"github.com/sirupsen/logrus"
	"iex-scrapper/helpers"
	"os"
)

// Get IEX CLoud client
func getClient() *iex.Client {
	token := os.Getenv("IEX_SECRET_TOKEN")
	client, err := iex.NewClient(token)
	if err != nil {
		helpers.MyLog.WithFields(logrus.Fields{
			"error": err.Error(),
		}).Error("IEXData Error")
	}
	return client
}
// Get Data from IEX cloud based on symbol
func GetIEXData(symbol string) (*iex.Quote, error) {
	client := getClient()
	quote, err := client.Quote(symbol, nil)
	if err != nil {
		helpers.MyLog.WithFields(logrus.Fields{
			"error": err.Error(),
		}).Error("IEXData Quote data get error")
	}
	return quote, err
}

// Get Logo of symbol from IEX cloud
// return string with logo of symbol
func GetLogo(symbol string) (string, error) {
	client := getClient()
	logo, er := client.Logo(symbol)
	if er != nil {
		return "", er
	}
	return logo.URL, nil
}
