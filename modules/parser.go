package modules

import (
	"github.com/sirupsen/logrus"
	"iex-scrapper/helpers"
	"iex-scrapper/structs"
	"sync"
)

func ScrapAndParse() {

	helpers.MyLog.WithFields(logrus.Fields{
		"func":    "ScrapAndParse",
		"message": "Start",
	}).Debug("Start ScrapAndParse")

	// get all companies

	var symbols []structs.Companies
	sql := `SELECT * FROM scraped_companies WHERE NOW() >= ( last_scraped + INTERVAL scrape_period SECOND )`

	helpers.DB.Unsafe().Select(&symbols,sql)

	var wg sync.WaitGroup
	wg.Add(len(symbols))

	for _, el := range symbols {
		go func(el structs.Companies) {
			defer wg.Done()

			// get data
			if data, er := GetIEXData(el.Symbol); er != nil {
				// insert to db
				sql := `insert into scraped_prices (company_id,scrape_time,current_price) values (:company_id,NOW(),:current_price) `
				helpers.DB.NamedExec(sql, map[string]interface{}{
					"company_id":    el.Id,
					"current_price": data.LatestPrice,
				})

				// some meta info update
				sql = `update scraped_companies set last_scraped = NOW() WHERE id = ? LIMIT 1`
				helpers.DB.Exec(sql, el.Id)

				if el.LogoURL == "" { // get logo of company
					logo, er := GetLogo(el.Symbol)
					if er == nil {
						el.UpdateLogo(logo)
					}
				}
			}
		}(el)
	}
	wg.Wait()
}
