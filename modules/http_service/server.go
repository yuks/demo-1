package http_service

import "github.com/gin-gonic/gin"

func Start() {
	r := gin.Default()
	r.GET("/getData", handleDataRetrieve)
	r.Run()
}
