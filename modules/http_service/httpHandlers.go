package http_service

import (
	valid "github.com/asaskevich/govalidator"
	"github.com/gin-gonic/gin"
	"iex-scrapper/modules"
)

type formData struct {
	Symbols []string `form:"symbol" valid:"required"`
}

func handleDataRetrieve(c *gin.Context) {

	data := formData{}
	c.Bind(&data)

	if ok, er := valid.ValidateStruct(data); !ok {
		c.JSON(200, gin.H{"result": false, "message": er.Error()})
		return
	}

	companiesData := modules.GetCompanies(modules.CompaniesFilter{
		Symbols: data.Symbols,
	})

	c.JSON(200, gin.H{"result": true, "companies": companiesData})
}
