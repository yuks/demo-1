module github.com/jonwho/go-iex

// go: no requirements found in Gopkg.lock

require (
	github.com/dnaeon/go-vcr v1.0.1
	github.com/google/go-querystring v1.0.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
